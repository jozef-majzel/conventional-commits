# Conventional Commits

## Prečo?

- Zdroj: <https://www.conventionalcommits.org/>
- Jendotná cesta pre sledovanie zmien v repozitároch
- Validné commit messages
- Semver friendly

## Formát

- Optional znamená, že táto časť commit message je voliteľná a teda validná commit message ju nemusí obsahovať.

```
<type>[optional scope]: <description>

[optional body]

[optional footer(s)]
```

- `verb: message`
- `verb(scope): message`

### Príklady

- `feat(.env.example): Add variables for Dynata API`

```
fix(ssr): Broken ssr rendering

Related #UXT-895
```

### Kľúčové slovesá (verb/type)

- `feat` - nová feature
- `fix` - bug fix
- `BREAKING CHANGE` - zásadna zmena
- `refactor` - zmeny kódu, ktoré nepridávajú novú funkcionalitu ani novú feature
- `docs` - zmeny v dokumentácii
- `build` - zmeny, ktoré ovplyvňujú build systému a dependencies (napr. npm, bit, ...)
- `deps`
- `ci` - zmeny k continuous-integration
- `chore` - drobné zmeny, ktoré sa inak nedajú zaradiť
- `perf` - kód zlepšujúci výkon a optimalizáciu apikácie
- `test` - vytvorenie, úprava testov
- `style` - zmeny, ktoré neovplyvňujú logiku kódu (white-space, formátovanie, bodkočiarky, ...)
- `revert` - revertovanie commitu (do footer je dobré uviesť SHAs commitov, ktoré sú revertované)
- `text` - zmeny, ktoré súvisia so zmenov textov, nápovedí, popisov, názvov tlačidiel, ...
- ...

### Scope (context)

**OPTIONAL!**

- K akej časti časti sa viaže commit

### Commit message (description)

- Prvý riadok by mal byť max 80 písmen
- Viacriadkový popis je OK

## Ako určiť release verziu na základe commitov?

### MAJOR vs MINOR vs PATCH

- `BREAKING CHANGE` -> MAJOR (1.0.0 -> 2.0.0)
- `feat` -> MINOR (1.0.0 -> 1.1.0)
- `fix` -> PATCH (1.0.0 -> 1.0.1)

### Release notes (changelog)

- Jednoducho generovateľný na základe commit messages
